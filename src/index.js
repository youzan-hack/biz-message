require('./check-hardware')
const config = require('./config.json')
const puppeteer = require('puppeteer')
const logger = require('./logger')

let count = 0
const fs = require('fs')
const csv = require('fast-csv')
const FileStore = require('file-store')
const store = FileStore(__dirname + '/store.txt')

let baseUrl = 'https://h5.youzan.com/v3/im/index?c=wsc&v=2&kdt_id='
let logiUrl = 'https://passport.youzan.com/settings'

let args = [
  '--no-sandbox',
  '--disable-setuid-sandbox',
  '--disable-infobars',
  '--window-position=0,0',
  '--ignore-certifcate-errors',
  '--ignore-certifcate-errors-spki-list',
  '--user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3312.0 Safari/537.36"'
]

const options = {
  args,
  headless: false,
  ignoreHTTPSErrors: true,
  devtools: false,
  userDataDir: './tmp'
}

const sendMessage = async (browser, id) => {
  logger.info(`send message to ${id}, count: ${++count} ...`)
  const page = await browser.newPage()
  try {
    await page.evaluateOnNewDocument(require('./preload'))
    await page.goto(baseUrl + id, {
      waitUntil: ['load']
    })
    await timeout(2)
    for (let index = 0; index < config.message.length; index++) {
      const message = config.message[index]
      await page.type('textarea', message)
      await timeout(1)
      await page.click('#send-btn')
      await timeout(1.5)
    }
    await timeout(1.5)
    if (config.file) {
      await page.click('.operation.plus')
      await timeout(0.5)
      const $input = await page.$('input[type="file"]')
      await $input.uploadFile(__dirname + config.file)
      await timeout(3)
    }
    await page.close()
  } catch (error) {
    console.error(error)
    logger.error(`send message failure: ${id}`)
    try {
      page.close()
    } catch (error) {}
  }
}

const getStoreIndex = () => {
  return new Promise((resolve, reject) => {
    store.get('index', function(err, value) {
      if (err || !value) {
        logger.debug("stor index can't found, from start")
        resolve(0)
      } else {
        logger.info('restore from ' + (value.index + 1))
        resolve(value.index + 1)
      }
    })
  })
}

const startPage = async browser => {
  logger.info('start send message...')
  let ids = []
  fs.createReadStream(__dirname + '/out.csv')
    .pipe(
      csv.parse({
        headers: false
      })
    )
    .on('data', row => {
      if (/^\d+$/.test(row[0])) {
        ids.push(row[0])
      }
    })
    .on('end', async () => {
      let index = await getStoreIndex()
      for (; index < ids.length; ) {
        let promises = []
        for (let j = 0; j < config.workers; j++) {
          if (index + j < ids.length) {
            const id = ids[index + j]
            promises.push(sendMessage(browser, id))
          }
        }
        await Promise.all(promises)
        index += config.workers
        store.set('index', { index })
      }
    })
}

const checkIsLogin = (browser, page) => {
  // 如果已经跳转到设置页面
  if (!page.url() || page.url().indexOf(logiUrl) !== 0) {
    setTimeout(() => {
      checkIsLogin(browser, page)
    }, 4000)
  } else {
    try {
      startPage(browser)
    } catch (error) {
      console.error(error)
    }
  }
}

const browserInit = async () => {
  const browser = await puppeteer.launch(options)
  const page = await browser.newPage()
  await page.evaluateOnNewDocument(require('./preload'))
  await page.goto(logiUrl, {
    waitUntil: ['load']
  })
  return {
    browser,
    page
  }
}
const timeout = time => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve()
    }, time * 1000)
  })
}

const start = async () => {
  let { browser, page } = await browserInit()
  await timeout(2)
  checkIsLogin(browser, page)
}

start()
