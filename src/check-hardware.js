const os = require('os')
const checkInfos = {
  hostname: 'PC-20160521YMXG',
  cpuModel: 'Intel(R) Core(TM) i5-5200U CPU @ 2.20GHz',
  mac: '1c:87:2c:b1:b9:94'
}

const hn = os.hostname()
const cpus = os.cpus()
const networksObj = os.networkInterfaces()

const check = () => {
  let cpuCheck = false
  let macCheck = false
  let hmCheck = hn === checkInfos.hostname

  cpus.forEach((cpu, idx, arr) => {
    if (!cpuCheck && cpu.model === checkInfos.cpuModel) {
      cpuCheck = true
    }
  })
  for (let nw in networksObj) {
    let objArr = networksObj[nw]
    objArr.forEach((obj, idx, arr) => {
      if (!macCheck && obj.mac === checkInfos.mac) {
        macCheck = true
      }
    })
  }
  if (!macCheck || !cpuCheck || !hmCheck) {
    console.log('Environment check failure, process exit')
    process.exit(0)
  }
}
check()
