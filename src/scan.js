// require('./check-hardware')
const config = require('./config.json')
const request = require('request')
const logger = require('./logger')
const csvWriter = require('csv-write-stream')
const fs = require('fs')
const cheerio = require('cheerio')
const FileStore = require('file-store')
const store = FileStore(__dirname + '/store.txt')

let baseUrl = 'https://shop43275400.youzan.com/v2/showcase/homepage?kdt_id='

const getShopInfo = async id => {
  console.log('scan: ' + id)
  return new Promise((resolve, reject) => {
    request(
      {
        url: baseUrl + id,
        followRedirect: false,
        timeout: 10000
      },
      function(error, response, body) {
        if (!response) {
          logger.error(id + ' 没有响应体')
        }
        if (!response || response.statusCode === 302) {
          return resolve({
            id,
            exist: 'false',
            type: '',
            link: baseUrl + id,
            name: ''
          })
        }
        if (error) {
          logger.error(`userid: ${id} doesn't exist`)
          console.error(error)
          return resolve(false)
        } else {
          let type = 'small'
          let exist = 'true'
          let state = 'serving'
          const $ = cheerio.load(body)
          if (
            $('.ft-links')
              .text()
              .includes('店铺主页')
          ) {
            type = 'big'
          }
          let name = $('meta[name="keywords"]')
            .attr('content')
            .split(' ')[0]
          if (
            $('.error-msg')
              .text()
              .includes('店铺不存在')
          ) {
            exist = false
          }
          if (
            $('.close-shop-container')
              .text()
              .includes('店铺暂停服务中，无法下单')
          ) {
            state = 'pause'
          }
          logger.info(`userid: ${id} exist:${exist}, state: ${state}`)
          return resolve({
            id: id,
            exist,
            type,
            link: baseUrl + id,
            name,
            state
          })
        }
      }
    )
  })
}
const getStoreScanIndex = () => {
  return new Promise((resolve, reject) => {
    store.get('scan', function(err, value) {
      if (err || !value) {
        logger.debug("store scan index can't found, from start")
        resolve(0)
      } else {
        logger.info('restore from ' + (value.index + 1))
        resolve(value.index + 1)
      }
    })
  })
}
const start = async () => {
  const writer = csvWriter({
    headers: undefined,
    sendHeaders: false
  })
  writer.pipe(
    fs.createWriteStream('out.csv', {
      flags: 'a+'
    })
  )
  let [startUserId, endUserId] = config.scanUserIds
  let index = startUserId
  let scanIndex = await getStoreScanIndex()
  console.log(scanIndex)
  if (startUserId <= scanIndex && scanIndex <= endUserId) {
    index = scanIndex
  }
  for (; index <= endUserId; index++) {
    let shopInfo = await getShopInfo(index)
    logger.info(`start write ${JSON.stringify(shopInfo)} to csv`)
    store.set('scan', { index })
    writer.write(shopInfo)
  }
  writer.end()
}

start()
