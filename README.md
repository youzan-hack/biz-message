### 基本信息

地址： https://h5.youzan.com/v3/im/index?c=wsc&v=2&kdt_id=43083235#/index
账号： 13846674547
密码： qqqq1111

```jsonc
{
  // 需要发消息的用户ID范围, 后一个比前一个大
  "scanUserIds": [43083235, 43081000],
  // 发送消息的内容
  "message": "你好，在么？我这边有个很好的产品，你是否要了解一下？",
  // 发送的图片文件名，记得以/开头，文件不要大太，方便发送
  "file": "/upload.jpg",
  // 同时进行数量
  "workers": 5
}
```

- 通过https://shop43275400.youzan.com/v2/showcase/homepage?kdt_id=43083250 判断店铺是否存在, 如果 302 则确认店铺不存在，
  - 批量扫描
  - 判断【店铺主页/小店主页】， 存店铺名字/链接
  - 生成一个文件 CSV
- 通过保存的 ID 批量发消息
